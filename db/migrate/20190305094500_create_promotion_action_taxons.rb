class CreatePromotionActionTaxons < ActiveRecord::Migration
  def self.up
    create_table :spree_promotion_action_taxons do |t|
      t.references :promotion_action
      t.references :taxon
    end

    add_foreign_key :spree_promotion_action_taxons, :spree_taxons, column: :taxon_id
    add_foreign_key :spree_promotion_action_taxon, :spree_promotion_actions, column: :promotion_action_id
    add_index :spree_promotion_action_taxons, [:promotion_action, :taxon], unique: true
  end

  def self.down
    drop_table :spree_promotion_action_taxons
  end
end
