# A rule to apply to an order's item quantity greater than (or greater than or equal to)
# a specific amount
module Spree
  class Promotion
    module Rules
      class TotalQuantity < PromotionRule
        preference :amount_min, :number, default: 0
        preference :operator_min, :string, default: '>'
        preference :amount_max, :number, default: 10
        preference :operator_max, :string, default: '<'

        OPERATORS_MIN = ['gt', 'gte']
        OPERATORS_MAX = ['lt','lte']

        def applicable?(promotable)
          promotable.is_a?(Spree::Order)
        end

        def eligible?(order, options = {})
          total_quantity = order.line_items.sum(&:quantity)

          lower_limit_condition = total_quantity.send(preferred_operator_min == 'gte' ? :>= : :>, preferred_amount_min.to_i)
          upper_limit_condition = total_quantity.send(preferred_operator_max == 'lte' ? :<= : :<, preferred_amount_max.to_i)

          eligibility_errors.add(:base, ineligible_message_max) unless upper_limit_condition
          eligibility_errors.add(:base, ineligible_message_min) unless lower_limit_condition

          eligibility_errors.empty?
        end

        private
        def formatted_amount_min
          Spree::Money.new(preferred_amount_min).to_s
        end

        def formatted_amount_max
          Spree::Money.new(preferred_amount_max).to_s
        end


        def ineligible_message_max
          if preferred_operator_max == 'gte'
            eligibility_error_message(:total_quantity_more_than_or_equal, amount: formatted_amount_max)
          else
            eligibility_error_message(:total_quantity_more_than, amount: formatted_amount_max)
          end
        end

        def ineligible_message_min
          if preferred_operator_min == 'gte'
            eligibility_error_message(:total_quantity_less_than, amount: formatted_amount_min)
          else
            eligibility_error_message(:total_quantity_less_than_or_equal, amount: formatted_amount_min)
          end
        end

      end
    end
  end
end
