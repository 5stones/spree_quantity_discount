module Spree
  class Promotion
    module Rules
      class UserRolesIncludes < PromotionRule
        preference :role_id, :number, default: nil

        def applicable?(promotable)
          promotable.is_a?(Spree::Order)
        end

        def eligible?(order, _options = {})
          if preferred_role_id.nil? || order.user.nil?
            return false
          end

          role = Spree::Role.find(preferred_role_id)

          unless order.user.spree_roles.include?(role)
            eligibility_errors.add(:base, eligibility_error_message(:no_user_specified))
          end

          return eligibility_errors.empty?
        end
      end
    end
  end
end
