module Spree
  class Promotion
    module Actions
      class CreateFilteredItemAdjustments < PromotionAction
        include Spree::CalculatedAdjustments
        include Spree::AdjustmentSource

        has_many :promotion_action_taxons, class_name: 'Spree::PromotionActionTaxon', foreign_key: :promotion_action_id
        has_many :taxons, through: :promotion_action_taxons
        accepts_nested_attributes_for :promotion_action_taxons

        # Creates a filtered adjustment that only applies to specific items.
        # Items can be excluded from the discount by providing filtered Taxons.
        #
        # This allows forthe configuration of a promotion that is _earned_ at the
        # order level, but _applied_ at the line item level

        before_validation -> { self.calculator ||= Calculator::PercentOnLineItem.new }

        def perform(options = {})
          order     = options[:order]
          promotion = options[:promotion]

          create_unique_adjustments(order, order.line_items) do |line_item|
            !product_excluded?(line_item.product) && promotion.line_item_actionable?(order, line_item)
          end
        end

        def compute_amount(line_item)
          return 0 unless !product_excluded?(line_item.product) && promotion.line_item_actionable?(line_item.order, line_item)

          amounts = [line_item.amount, compute(line_item)]
          order   = line_item.order

          # Prevent negative order totals
          amounts << order.amount - order.adjustments.sum(:amount).abs if order.adjustments.any?

          amounts.min * -1
        end

        # Return true if the individual item is eligible for the discount
        def product_excluded?(product)
          taxons.each do |taxon|
            return true if product.taxons.include? taxon
          end

          return false
        end

      end
    end
  end
end
