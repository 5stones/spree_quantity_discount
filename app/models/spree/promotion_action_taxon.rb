module Spree
  class PromotionActionTaxon < Spree::Base
    belongs_to :promotion_action, class_name: 'Spree::Promotion::Actions::CreateFilteredItemAdjustments'
    belongs_to :taxon, class_name: 'Spree::Taxon'

    validates :promotion_action, :taxon, presence: true
  end
end
